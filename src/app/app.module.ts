import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FlxuiComponent } from './component/flxui/flxui.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MockServerResultsService } from './model/mock-server-results-service';


@NgModule({
  declarations: [
    AppComponent,
   FlxuiComponent,
   
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxDatatableModule
  ],
  providers: [MockServerResultsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
