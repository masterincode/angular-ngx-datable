import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlxuiComponent } from './flxui.component';

describe('FlxuiComponent', () => {
  let component: FlxuiComponent;
  let fixture: ComponentFixture<FlxuiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlxuiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlxuiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
