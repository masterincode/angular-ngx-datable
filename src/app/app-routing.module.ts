import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlxuiComponent } from './component/flxui/flxui.component';

const routes: Routes = [

  { path:'**' , component: FlxuiComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
